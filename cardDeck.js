/**
 * Returns an array of 52 Cards
 * @returns {Array} deck - a deck of cards
 */
const getDeck = function() {
  const suits = ['hearts','diamonds','♠','clubs'];
  const displayValue = [2,3,4,5,6,7,8,9,10,'Jack','Queen','King','Ace'];
  const value = [2,3,4,5,6,7,8,9,10,10,10,10,11];
  let cards = [];
  for(let i=0;i<suits.length;i++) {
  	for(let j=0; j<value.length;j++) { 
    	var card = { suit: suits[i], displayVal:displayValue[j], val:value[j] }
      cards.push(card);
    }
  }
  return cards;
}

// CHECKS
const deck = getDeck();
console.log(`Deck length equals 52? ${deck.length === 52}`);

const randomCard = deck[Math.floor(Math.random() * 52)];

const cardHasVal = randomCard && randomCard.val && typeof randomCard.val === 'number';
console.log(`Random card has val? ${cardHasVal}`);

const cardHasSuit = randomCard && randomCard.suit && typeof randomCard.suit === 'string';
console.log(`Random card has suit? ${cardHasSuit}`);

const cardHasDisplayVal = randomCard && 
    randomCard.displayVal && 
    typeof randomCard.displayVal === 'string';
console.log(`Random card has display value? ${cardHasDisplayVal}`);