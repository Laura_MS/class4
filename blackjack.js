const blackjackDeck = getDeck();

/**
 * Represents a card player (including dealer).
 * @constructor
 * @param {string} name - The name of the player
 */
const CardPlayer = function(name) {
  this.name = name;
  this.hand = [];
  this.drawCard = function() {
    let randomNum = Math.floor(Math.random()*52);
    this.hand.push(blackjackDeck[randomNum]);
  }
};

// CREATE TWO NEW CardPlayers
// why don't these work?
var dealer = CardPlayer("Dealer");
var player = CardPlayer("Laura");

/**
 * Calculates the score of a Blackjack hand
 * @param {Array} hand - Array of card objects with val, displayVal, suit properties
 * @returns {Object} - Object containing Total points and whether hand isSoft
 */
const calcPoints = function(hand) {
  let points = 0, softness = false;
  for(var i=0; i<hand.length; i++) {
      if(hand[i].displayVal==="Ace") {
        if(points > 10) { //treat ace as 1
          points += 1;
        }
        else points += 11;
        softness = true;
      }
      else {
        points += hand[i].val;
      }
  }
  return { total:points, isSoft:softness }
}

/**
 * Determines whether the dealer should draw another card
 * @param {Array} dealerHand Array of card objects with val, displayVal, suit properties
 * @returns {boolean} whether dealer should draw another card
 */
const dealerShouldDraw = function(dealerHand) {
  if(calcPoints(dealerHand).totalPoints <= 16) {
    return true;
  }

  if(calcPoints(dealerHand).totalPoints == 17 && calcPoints(dealerHand).softness) {
    return true;
  }

  return false;
}

/**
 * Determines the winner if both player and dealer stand
 * @param {number} playerScore 
 * @param {number} dealerScore 
 * @returns {string} States the player's score, the dealer's score, and who wins
 */
const determineWinner = function(playerScore, dealerScore) {
  let returnString;
  if(dealerScore > 21) { return `Player's score is ${playerScore}. Dealer's score is ${dealerScore}. Player wins!`; }
  else if(playerScore > 21) { return `Player's score is ${playerScore}. Dealer's score is ${dealerScore}. Dealer wins!`; }
  else if(playerScore > dealerScore) { return `Player's score is ${playerScore}. Dealer's score is ${dealerScore}. Player wins!`; }
  else if(playerScore < dealerScore) { return `Player's score is ${playerScore}. Dealer's score is ${dealerScore}. Dealer wins!`; }
  return `Player's score is ${playerScore}. Dealer's score is ${dealerScore}. Tie game!`;
}

/**
 * Creates user prompt to ask if they'd like to draw a card
 * @param {number} count 
 * @param {string} dealerCard 
 */
const getMessage = function(count, dealerCard) {
  return `Dealer showing ${dealerCard.displayVal}, your count is ${count}.  Draw card?`
}

/**
 * Logs the player's hand to the console
 * @param {CardPlayer} player 
 */
const showHand = function(player) {
  let displayHand = player.hand.map(function(card) { return card.displayVal});

  //from https://www.w3schools.com/jsref/met_node_appendchild.asp
  let node;
  let textnode;

  node = document.createElement("p");
  textnode = document.createTextNode(player.name+"'s hand is "+displayHand.join(', '));
  node.appendChild(textnode);
  document.getElementById("gameLog").appendChild(node);

  node = document.createElement("p");
  textnode = document.createTextNode("Total: "+calcPoints(player.hand).total);
  node.appendChild(textnode);
  document.getElementById("gameLog").appendChild(node);
}

/**
 * Runs Blackjack Game
 */
const startGame = function() {
  var player = new CardPlayer("Laura");
  var dealer = new CardPlayer("Dealer");
  player.drawCard();
  dealer.drawCard();
  player.drawCard();
  dealer.drawCard();

  //player logic
  let playerScore = calcPoints(player.hand).total;
  showHand(player);
  while (playerScore < 21 && confirm(getMessage(playerScore, dealer.hand[0]))) {
    player.drawCard();
    playerScore = calcPoints(player.hand).total;
    showHand(player);
  }
  if (playerScore > 21) {
    return 'You went over 21 - you lose!';
  }
  console.log(`Player stands at ${playerScore}`);

  //dealer logic
  let dealerScore = calcPoints(dealer.hand).total;
  showHand(dealer);

  while(dealerScore < 17 || (dealerScore==17 && calcPoints(dealer).isSoft) ) {
    dealer.drawCard();
    dealerScore = calcPoints(dealer.hand).total;
    showHand(dealer);
  }

  console.log(`Dealer stands at ${playerScore}`);

  return determineWinner(playerScore, dealerScore);
}
console.log(startGame());